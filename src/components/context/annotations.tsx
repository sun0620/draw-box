import React, { createContext, PropsWithChildren, useState, useContext } from 'react'

export interface BoundingBox {
  x: number;
  y: number;
  width: number;
  height: number;
  id: number;
}

export const getMatrix = (boundingBox: BoundingBox) => {
  return [
    [boundingBox.x, boundingBox.y],
    [boundingBox.x + boundingBox.width, boundingBox.y],
    [boundingBox.x, boundingBox.y + boundingBox.height],
    [boundingBox.x + boundingBox.width, boundingBox.y + boundingBox.height],
  ]
}

interface AnnotationsContext {
  annotations: BoundingBox[];
  newAnnotation: BoundingBox[];
  setAnnotations: React.Dispatch<React.SetStateAction<BoundingBox[]>>;
  setNewAnnotation: React.Dispatch<React.SetStateAction<BoundingBox[]>>;
}

const noop = () => { }

export const annotationsContext = createContext<AnnotationsContext>({
  annotations: [],
  newAnnotation: [],
  setAnnotations: noop,
  setNewAnnotation: noop,
})

export const AnnotationsProvider: React.FC<PropsWithChildren<Record<string, unknown>>> = ({ children }) => {
  const [annotations, setAnnotations] = useState<BoundingBox[]>([]);
  const [newAnnotation, setNewAnnotation] = useState<BoundingBox[]>([]);

  return (
    <annotationsContext.Provider value={{
      annotations,
      newAnnotation,
      setAnnotations,
      setNewAnnotation,
    }}>
      {children}
    </annotationsContext.Provider>
  )
}

export function useAnnotations() {
  return useContext(annotationsContext);
}