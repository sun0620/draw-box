import { useState, createContext, FC, PropsWithChildren, Dispatch, SetStateAction, useContext } from 'react'

interface StageContext {
	stageOffsetX: number;
	stageOffsetY: number;
	scale: number;
	setScale: Dispatch<SetStateAction<number>>;
	setStageOffsetX: Dispatch<SetStateAction<number>>;
	setStageOffsetY: Dispatch<SetStateAction<number>>;
}

const noop = () => {}

export const stageContext = createContext<StageContext>({
	stageOffsetX: 0,
	stageOffsetY: 0,
	scale: 1,
	setScale: noop,
	setStageOffsetX: noop,
	setStageOffsetY: noop,
})

export const StageProvider: FC<PropsWithChildren<Record<string, unknown>>> = ({ children }) => {
	const [scale, setScale] = useState(1);
	const [stageOffsetX, setStageOffsetX] = useState(0);
	const [stageOffsetY, setStageOffsetY] = useState(0);
	
	return (
		<stageContext.Provider value={{
			scale,
			stageOffsetX,
			stageOffsetY,
			setScale,
			setStageOffsetX,
			setStageOffsetY
		}}>
			{children}
		</stageContext.Provider>
	)
}

export function useStage() {
	return useContext(stageContext);
}