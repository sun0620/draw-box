import { useState, createContext, FC, PropsWithChildren, useContext } from 'react'

type Status = "move" | "drag" | "draw" | "grab";

interface MouseStatusContext {
  status: Status;
	setStatus: (nextStatus: Status) => void;
  restoreStatus: () => void;
}

const noop = () => {}

export const mouseStatusContext = createContext<MouseStatusContext>({
	status: "move",
	setStatus: noop,
  restoreStatus: noop,
})

export const MouseStatusProvider: FC<PropsWithChildren<Record<string, unknown>>> = ({ children }) => {
	const [status, setStatusLocal] = useState<Status>("move"); // TODO: setStatusLocal = bad bad name
	const [lastStatus, setLastStatus] = useState<Status>(status);

	return (
		<mouseStatusContext.Provider value={{
			status,
			setStatus: (nextStatus: Status) => {
        setLastStatus(status)
        setStatusLocal(nextStatus)
      },
      restoreStatus: () => {
        setStatusLocal(lastStatus)
      }
		}}>
			{children}
		</mouseStatusContext.Provider>
	)
}

export function useMouseStatus() {
	return useContext(mouseStatusContext);
}