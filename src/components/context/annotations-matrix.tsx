import React, { createContext, PropsWithChildren, useState, useContext } from 'react'

export interface AnnotationsMatrix {
  points: number[][];
}

export interface NewAnnotationsMatrix extends AnnotationsMatrix{
  currentCursor: number[];
  isMouseOverStartPoint: boolean;
}

export const newAnnotationsMatrixInitState: NewAnnotationsMatrix = {
  currentCursor:[],
  points:[],
  isMouseOverStartPoint: false
}

interface AnnotationsMatrixContext {
  annotationsMatrices: AnnotationsMatrix[];
  newAnnotationMatrix: NewAnnotationsMatrix;
  setAnnotationsMatrices: React.Dispatch<React.SetStateAction<AnnotationsMatrix[]>>;
  setNewAnnotationMatrix: React.Dispatch<React.SetStateAction<NewAnnotationsMatrix>>;
}

const noop = () => { }

export const annotationsMatrixContext = createContext<AnnotationsMatrixContext>({
  annotationsMatrices: [],
  newAnnotationMatrix: newAnnotationsMatrixInitState,
  setAnnotationsMatrices: noop,
  setNewAnnotationMatrix: noop,
})

export const AnnotationsMatrixProvider: React.FC<PropsWithChildren<Record<string, unknown>>> = ({ children }) => {
  const [annotationsMatrices, setAnnotationsMatrices] = useState<AnnotationsMatrix[]>([]);
  const [newAnnotationMatrix, setNewAnnotationMatrix] = useState<NewAnnotationsMatrix>(newAnnotationsMatrixInitState);

  return (
    <annotationsMatrixContext.Provider value={{
      annotationsMatrices,
      newAnnotationMatrix,
      setAnnotationsMatrices,
      setNewAnnotationMatrix,
    }}>
      {children}
    </annotationsMatrixContext.Provider>
  )
}

export function useAnnotationsMatrix() {
  return useContext(annotationsMatrixContext);
}