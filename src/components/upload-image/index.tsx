import React, {  } from 'react';
import { Dropzone, DropzoneStatus, IMAGE_MIME_TYPE } from '@mantine/dropzone';
import { Group, Text, useMantineTheme, MantineTheme } from '@mantine/core';
import { ImageIcon, UploadIcon, CrossCircledIcon } from '@modulz/radix-icons';
import { IconProps } from '@modulz/radix-icons/dist/types';

interface UploadImageProps {
  setImageUri: React.Dispatch<React.SetStateAction<string>>;
}

export const UploadImage: React.FC<UploadImageProps> = (props) => {
  const { setImageUri } = props;
  const theme = useMantineTheme();

  function ImageUploadIcon(props: IconProps & { status: DropzoneStatus }) {
    const { status, ...rest } = props;
    if (status.accepted) {
      return <UploadIcon {...rest} />;
    }
  
    if (status.rejected) {
      return <CrossCircledIcon {...rest} />;
    }
  
    return <ImageIcon {...rest} />;
  }
  
  function getIconColor(status: DropzoneStatus, theme: MantineTheme) {
    return status.accepted
      ? theme.colors[theme.primaryColor][6]
      : status.rejected
      ? theme.colors.red[6]
      : theme.colorScheme === 'dark'
      ? theme.colors.dark[0]
      : theme.black;
  }

  return (
    // See results in console after dropping files to Dropzone
    <Dropzone
      onDrop={(files) => setImageUri(URL.createObjectURL(files[0]))}
      onReject={(files) => console.log('rejected files', files)}
      maxSize={3 * 1024 ** 2}
      accept={IMAGE_MIME_TYPE}
    >
      {(status) => (
        <Group position="center" spacing="xl" style={{ minHeight: 220, pointerEvents: 'none' }}>
          <ImageUploadIcon
            status={status}
            style={{ width: 80, height: 80, color: getIconColor(status, theme) }}
          />
          <div>
            <Text size="xl" inline>
              Drag images here or click to select files
            </Text>
            <Text size="sm" color="dimmed" inline mt={7}>
              Attach as many files as you like, each file should not exceed 5mb
            </Text>
          </div>
        </Group>
      )}
    </Dropzone>
  );
}