import { KonvaEventObject } from "konva/lib/Node";
import { useStage } from '../../context/stage';
import { useAnnotationsMatrix, newAnnotationsMatrixInitState } from '../../context/annotations-matrix';
import { useMouseStatus } from '../../context/mouseStatus';
import { setMouseStyle, restoreLastMouseStyle } from '../../helper/common';

const LEFT_BUTTON = 0;
// const WHEEL_BUTTON = 1;
const RIGHT_BUTTON = 2;

export function useMouse() {
  const { stageOffsetX, stageOffsetY, scale, setStageOffsetX, setStageOffsetY } = useStage();
  const { annotationsMatrices, newAnnotationMatrix, setAnnotationsMatrices, setNewAnnotationMatrix } = useAnnotationsMatrix();
  const { status, setStatus, restoreStatus } = useMouseStatus();

  const onMouseDown = (event: KonvaEventObject<MouseEvent>) => {
    if (event.evt.button === LEFT_BUTTON) {
      const { points, isMouseOverStartPoint } = newAnnotationMatrix;

      const x = event.target.getStage()?.getPointerPosition()?.x;
      const y = event.target.getStage()?.getPointerPosition()?.y;

      if (x && y && (status === "move" || status === "draw")) {
        if (isMouseOverStartPoint && points.length >= 3) {
          setStatus("move");
          setAnnotationsMatrices([...annotationsMatrices, {points:[...newAnnotationMatrix.points]}]);
          setNewAnnotationMatrix(newAnnotationsMatrixInitState);
        } else {
          setStatus("draw");
          setNewAnnotationMatrix({
            ...newAnnotationMatrix,
            points: [...newAnnotationMatrix.points, [(x - stageOffsetX) / scale, (y - stageOffsetY) / scale]]
          });
        }
      }
    }
  };

  const onMouseUp = (event: KonvaEventObject<MouseEvent>) => {
    if (status === "grab") {
      restoreStatus();
      restoreLastMouseStyle(event);
    }
  };

  const onMouseMove = (event: KonvaEventObject<MouseEvent>) => {
    if (event.evt.button === LEFT_BUTTON && status === "draw") {
      const x = event.target.getStage()?.getPointerPosition()?.x;
      const y = event.target.getStage()?.getPointerPosition()?.y;
      if (x && y) {
        setNewAnnotationMatrix({
          ...newAnnotationMatrix,
          currentCursor: [(x - stageOffsetX) / scale, (y - stageOffsetY) / scale]
        });
      }
    } else if (event.evt.button === RIGHT_BUTTON && status === "grab") {
      const { movementX, movementY } = event.evt;

      setStageOffsetX(stageOffsetX + movementX);
      setStageOffsetY(stageOffsetY + movementY);
    }
  };

  const onContextMenu = (event: KonvaEventObject<MouseEvent>) => {
    event.evt.preventDefault();

    setStatus("grab");
    setMouseStyle(event, 'grab');
  };

  return { onMouseDown, onMouseUp, onMouseMove, onContextMenu }
}