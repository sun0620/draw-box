import { KonvaEventObject } from "konva/lib/Node";
import { useStage } from '../../context/stage';

const SCALE_BY = 1.2;

export function useZoom() {
	const { stageOffsetX, stageOffsetY, scale, setStageOffsetX, setStageOffsetY, setScale } = useStage();
	
	const onWheel = (event: KonvaEventObject<WheelEvent>) => { 
		event.evt.preventDefault();
		const { offsetX, offsetY } = event.evt;
		const mousePointTo = {
			x: offsetX / scale - stageOffsetX / scale,
			y: offsetY / scale - stageOffsetY / scale
		};
		const newScale = event.evt.deltaY < 0 ? scale * SCALE_BY : scale / SCALE_BY;

		setScale(newScale);
		setStageOffsetX((offsetX / newScale - mousePointTo.x) * newScale);
		setStageOffsetY((offsetY / newScale - mousePointTo.y) * newScale);
	};

	return {onWheel}
}