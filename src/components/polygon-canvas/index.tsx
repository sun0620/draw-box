import React, {  } from "react";
import { Image, Layer, Stage } from "react-konva";
import { useStage, stageContext } from '../context/stage';
import { useMouseStatus, mouseStatusContext } from '../context/mouseStatus';
import { useAnnotationsMatrix, annotationsMatrixContext } from '../context/annotations-matrix';
import { useZoom } from './hooks/useZoom';
import { useMouse } from './hooks/useMouse';
import { useMouseStyle } from './hooks/useMouseStyle';
import { Polygon } from '../polygon'

interface CanvasProps {
  image: HTMLImageElement | undefined;
}

const { Provider: StageProvider } = stageContext;
const { Provider: MouseStatusProvider } = mouseStatusContext;
const { Provider: AnnotationsMatrixProvider } = annotationsMatrixContext;

export const PolygonCanvas: React.FC<CanvasProps> = (props) => {
  const { image } = props;

  const stageContext = useStage();
  const { stageOffsetX, stageOffsetY, scale } = stageContext;
  const mouseStatusContext = useMouseStatus();
  const annotationsMatrixContext = useAnnotationsMatrix();

  const clickHandlers = useMouse();
  const zoomHandlers = useZoom();
  const mouseStyleHandler = useMouseStyle();

  return (
    <Stage
      width={image?.width ? image?.width : 0}
      height={image?.height ? image?.height : 0}
      scaleX={scale}
      scaleY={scale}
      x={stageOffsetX}
      y={stageOffsetY}
      {...clickHandlers}
      {...zoomHandlers}
      {...mouseStyleHandler}
    >
      <StageProvider value={stageContext}>
        <MouseStatusProvider value={mouseStatusContext}>
          <AnnotationsMatrixProvider value={annotationsMatrixContext}>
            <Layer>
              <Image image={image} />
              <Polygon/>
            </Layer>
          </AnnotationsMatrixProvider>
        </MouseStatusProvider>
      </StageProvider>
    </Stage>
  );
};
