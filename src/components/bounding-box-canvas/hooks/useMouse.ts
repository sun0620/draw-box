import { KonvaEventObject } from "konva/lib/Node";
import { useStage } from '../../context/stage';
import { useAnnotations } from '../../context/annotations';
import { useMouseStatus } from '../../context/mouseStatus';
import { setMouseStyle, restoreLastMouseStyle } from '../../helper/common';

const LEFT_BUTTON = 0;
// const WHEEL_BUTTON = 1;
const RIGHT_BUTTON = 2;

export function useMouse() {
  const { stageOffsetX, stageOffsetY, scale, setStageOffsetX, setStageOffsetY } = useStage();
  const { annotations, newAnnotation, setAnnotations, setNewAnnotation } = useAnnotations();
  const { status, setStatus } = useMouseStatus();

  const onMouseDown = (event: KonvaEventObject<MouseEvent>) => {
    if (event.evt.button === LEFT_BUTTON && status === "move") {
      if (newAnnotation.length === 0) {
        const { offsetX, offsetY } = event.evt;
        setStatus("draw");
        setNewAnnotation([
          {
            x: (offsetX - stageOffsetX) / scale,
            y: (offsetY - stageOffsetY) / scale,
            width: 0,
            height: 0,
            id: 0
          }
        ]);
      }
    }
  };

  const onMouseUp = (event: KonvaEventObject<MouseEvent>) => {
    if (event.evt.button === LEFT_BUTTON && status === "draw") {
      if (newAnnotation.length === 1) {
        const sx = newAnnotation[0].x;
        const sy = newAnnotation[0].y;
        const { offsetX, offsetY } = event.evt;
        const annotationToAdd = {
          x: sx,
          y: sy,
          width: (offsetX - stageOffsetX) / scale - sx,
          height: (offsetY - stageOffsetY) / scale - sy,
          id: annotations.length + 1
        };
        annotations.push(annotationToAdd);

        setStatus("drag");
        setMouseStyle(event, 'move');
        setNewAnnotation([]);
        setAnnotations(annotations);
      }
    } else if (status === "grab") {
      restoreLastMouseStyle(event);
    }
  };

  const onMouseMove = (event: KonvaEventObject<MouseEvent>) => {
    if (event.evt.button === LEFT_BUTTON && status === "draw") {
      if (newAnnotation.length === 1) {
        const sx = newAnnotation[0].x;
        const sy = newAnnotation[0].y;
        const { offsetX, offsetY } = event.evt;

        setNewAnnotation([
          {
            x: sx,
            y: sy,
            width: (offsetX - stageOffsetX) / scale - sx,
            height: (offsetY - stageOffsetY) / scale - sy,
            id: 0
          }
        ]);
      }
    } else if (event.evt.button === RIGHT_BUTTON) {
      const { movementX, movementY } = event.evt;

      setStageOffsetX(stageOffsetX + movementX);
      setStageOffsetY(stageOffsetY + movementY);
    }
  };

  const onContextMenu = (event: KonvaEventObject<MouseEvent>) => {
    event.evt.preventDefault();

    setStatus("grab");
    setMouseStyle(event, 'grab');
  };

  return { onMouseDown, onMouseUp, onMouseMove, onContextMenu }
}