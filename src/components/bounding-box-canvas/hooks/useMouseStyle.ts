import { KonvaEventObject } from "konva/lib/Node";
import { setMouseStyle } from '../../helper/common';

export function useMouseStyle() {

  const onMouseEnter = (event: KonvaEventObject<MouseEvent>) => {
    setMouseStyle(event, 'crosshair');
  }
  const onMouseLeave = (event: KonvaEventObject<MouseEvent>) => {
    setMouseStyle(event, 'default');
  }

  return { onMouseEnter, onMouseLeave }
}