import React, {  } from "react";
import { Group, Image, Layer, Stage } from "react-konva";
import { useStage, stageContext } from '../context/stage';
import { useAnnotations, annotationsContext } from '../context/annotations';
import { useMouseStatus, mouseStatusContext } from '../context/mouseStatus';
import { useZoom } from './hooks/useZoom';
import { useMouse } from './hooks/useMouse';
import { useMouseStyle } from './hooks/useMouseStyle';
import { BoundingBoxes } from "../bounding-box";

interface CanvasProps {
  image: HTMLImageElement | undefined;
}

const { Provider: StageProvider } = stageContext;
const { Provider: AnnotationsProvider } = annotationsContext;
const { Provider: MouseStatusProvider } = mouseStatusContext;

export const BoundingBoxCanvas: React.FC<CanvasProps> = (props) => {
  const { image } = props;

  const stageContext = useStage();
  const { stageOffsetX, stageOffsetY, scale } = stageContext;
  const annotationsContext = useAnnotations();
  const { annotations, newAnnotation } = annotationsContext;
  const mouseStatusContext = useMouseStatus();

  const mouseHandler = useMouse();
  const zoomHandler = useZoom();
  const mouseStyleHandler = useMouseStyle();

  const annotationsToDraw = [...annotations, ...newAnnotation];

  return (
    <Stage
      width={image?.width ? image?.width : 0}
      height={image?.height ? image?.height : 0}
      scaleX={scale}
      scaleY={scale}
      x={stageOffsetX}
      y={stageOffsetY}
      {...mouseHandler}
      {...zoomHandler}
      {...mouseStyleHandler}
    >
      <StageProvider value={stageContext}>
        <MouseStatusProvider value={mouseStatusContext}>
          <AnnotationsProvider value={annotationsContext}>
            <Layer>
              <Image image={image} />
              <Group>
                {annotationsToDraw.map(value => {
                  return <BoundingBoxes {...value}/>
                })}
              </Group>
            </Layer>
          </AnnotationsProvider>
        </MouseStatusProvider>
      </StageProvider>
    </Stage>
  );
};
