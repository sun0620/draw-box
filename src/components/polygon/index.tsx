import React from 'react';
import { Group, Rect, Line as KonvaLine } from 'react-konva';
import { useHotkeys } from 'react-hotkeys-hook';
import { useStage } from '../context/stage';
import {
  useAnnotationsMatrix,
  AnnotationsMatrix,
} from '../context/annotations-matrix';
import { useMouse } from './hooks/useMouse';
import { Lines } from '../lines';
import { Points } from '../points';
import { flatten } from '../helper/common';

export const Polygon: React.FC<{}> = () => {
  const { scale } = useStage();
  const { annotationsMatrices, newAnnotationMatrix } = useAnnotationsMatrix();
  const { onMouseOverNewAnnotation, onMouseOutNewAnnotation } = useMouse();

  useHotkeys('shift', () => {}); // TODO: for straight line

  return (
    <>
      {annotationsMatrices.map((value: AnnotationsMatrix, index: number) => {
        console.log(value.points);
        return (
          <Group>
            <Lines index={index} points={value.points} />
            <Points index={index} points={value.points} />
          </Group>
        );
      })}
      <Group>
        <KonvaLine
          points={[
            ...flatten(newAnnotationMatrix.points),
            ...newAnnotationMatrix.currentCursor,
          ]}
          stroke="black"
          strokeWidth={1 / scale}
          closed={false}
        />
        {newAnnotationMatrix.points.map((point: number[], index: number) => {
          const width = 6 / scale;
          const x = point[0] - width / 2;
          const y = point[1] - width / 2;
          const startPointAttr =
            index === 0
              ? {
                  hitStrokeWidth: 12 / scale,
                  onMouseOver: onMouseOverNewAnnotation,
                  onMouseOut: onMouseOutNewAnnotation,
                }
              : null;

          return (
            <Rect
              key={index}
              x={x}
              y={y}
              width={width}
              height={width}
              fill="white"
              stroke="black"
              strokeWidth={1 / scale}
              draggable
              {...startPointAttr}
            />
          );
        })}
      </Group>
    </>
  );
};
