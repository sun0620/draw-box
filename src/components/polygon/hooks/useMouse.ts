import { useAnnotationsMatrix } from '../../context/annotations-matrix';
import { KonvaEventObject } from "konva/lib/Node";

export function useMouse() {
  const { newAnnotationMatrix, setNewAnnotationMatrix }  = useAnnotationsMatrix();

  const onMouseOverNewAnnotation = (event: KonvaEventObject<MouseEvent>) => {
    if (newAnnotationMatrix.points.length !== 0) {
      if (newAnnotationMatrix.points.length < 3) return;
      setNewAnnotationMatrix({...newAnnotationMatrix, isMouseOverStartPoint: true});
    }
    event.target.scale({ x: 2, y: 2 });
  };

  const onMouseOutNewAnnotation = (event: KonvaEventObject<MouseEvent>) => {
    event.target.scale({ x: 1, y: 1 });
    setNewAnnotationMatrix({...newAnnotationMatrix, isMouseOverStartPoint: false});
  };

  return { onMouseOverNewAnnotation, onMouseOutNewAnnotation }
}