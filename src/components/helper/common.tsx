import { KonvaEventObject } from "konva/lib/Node";

type Style = 'default' | 'move' | 'crosshair' | 'grab';
let lastMouseStyle: Style = 'default';

export const setMouseStyle = (event: KonvaEventObject<MouseEvent>, style: Style) => {
  const container = event.target.getStage()?.container();
  if (container) {
    lastMouseStyle = container.style.cursor as Style;
    container.style.cursor = style;
  }
}

export const restoreLastMouseStyle = (event: KonvaEventObject<MouseEvent>) => {
  const container = event.target.getStage()?.container();
  if (container) {
    container.style.cursor = lastMouseStyle;
  }
}

export const flatten = (points: number[][]) => {
  return points.reduce((a: number[], b: number[]) => a.concat(b), []);
}