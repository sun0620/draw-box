import React from 'react';
import { Line as KonvaLine } from 'react-konva';
// import { useHotkeys } from 'react-hotkeys-hook';
import { useStage } from '../context/stage';
import {
  useAnnotationsMatrix
} from '../context/annotations-matrix';
import { useMouse } from './hooks/useMouse';
import { flatten } from '../helper/common';

interface LineProps {
  index: number;
  points: number[][];
}

export const Lines: React.FC<LineProps> = (props) => {
  const { index, points } = props;
  const { scale } = useStage();
  const { annotationsMatrices, setAnnotationsMatrices } =
    useAnnotationsMatrix();
  const mouseHandler = useMouse();

  return (
    <KonvaLine
      _useStrictMode={true}
      x={0}
      y={0}
      key={index}
      points={flatten(points)}
      stroke="black"
      strokeWidth={1 / scale}
      closed={true}
      fill={'rgba(0,255,0,0.1'}
      draggable={true}
      onDragMove={(event) => {
        const mov = [event.evt.movementX, event.evt.movementY];
        setAnnotationsMatrices([
          ...annotationsMatrices.slice(0, index),
          {
            points: points.map((points: number[]) => {
              return [points[0] + mov[0] / scale, points[1] + mov[1] / scale];
            }),
          },
          ...annotationsMatrices.slice(index + 1),
        ]);
      }}
      {...mouseHandler}
    />
  );
};