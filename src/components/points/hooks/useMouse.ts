import { useMouseStatus } from '../../context/mouseStatus';
import { setMouseStyle, restoreLastMouseStyle } from '../../helper/common';
import { KonvaEventObject } from "konva/lib/Node";

export function useMouse() {
  const { status, setStatus } = useMouseStatus();

  const onMouseOver = (event: KonvaEventObject<MouseEvent>) => {
    if (status !== "draw") {
      setStatus("drag");
      setMouseStyle(event, 'move');
      event.target.scale({ x: 2, y: 2 });
    }
  };

  const onMouseOut = (event: KonvaEventObject<MouseEvent>) => {
    if (status !== "draw") {
      setStatus("move");
      restoreLastMouseStyle(event);
      event.target.scale({ x: 1, y: 1 });
    }
  };

  return { onMouseOver, onMouseOut }
}