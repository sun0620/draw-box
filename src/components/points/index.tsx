import React from 'react';
import { Rect } from 'react-konva';
// import { useHotkeys } from 'react-hotkeys-hook';
import { useStage } from '../context/stage';
import { useAnnotationsMatrix } from '../context/annotations-matrix';
import { useMouse } from './hooks/useMouse';

interface PointsProps {
  index: number;
  points: number[][];
}

export const Points: React.FC<PointsProps> = (props) => {
  const { index, points } = props;
  const { scale } = useStage();
  const { annotationsMatrices, setAnnotationsMatrices } =
    useAnnotationsMatrix();
  const mouseHandler = useMouse();

  return <>
    {points.map((points: number[], pointsIndex: number) => {
      const width = 6 / scale;
      const x = points[0] - width / 2;
      const y = points[1] - width / 2;

      return (
        <Rect
          key={pointsIndex}
          x={x}
          y={y}
          width={width}
          height={width}
          fill="white"
          stroke="black"
          strokeWidth={1 / scale}
          draggable={true}
          onDragMove={(event) => {
            const pos = [event.target.attrs.x, event.target.attrs.y];

            setAnnotationsMatrices([
              ...annotationsMatrices.slice(0, index),
              {
                points: [
                  ...annotationsMatrices[index].points.slice(0, pointsIndex),
                  pos,
                  ...annotationsMatrices[index].points.slice(pointsIndex + 1),
                ],
              },
              ...annotationsMatrices.slice(index + 1),
            ]);
          }}
          {...mouseHandler}
        />
      );
    })}
  </>
};
