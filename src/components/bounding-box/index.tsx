import React, {  } from "react";
import { Rect, Transformer } from "react-konva";
import { useAnnotations, BoundingBox } from '../context/annotations';
import { useStage } from '../context/stage';
import { useMouseStyle as useRectMouseStyle } from './hooks/useRectMouseStyle';
import { useMouseStyle as useTransMouseStyle } from './hooks/useTransMouseStyle';

export const BoundingBoxes: React.FC<BoundingBox> = (props) => {
  const { x, y, width, height, id } = props;

  const { annotations, setAnnotations } = useAnnotations()
  const { stageOffsetX, stageOffsetY, scale } = useStage();

  const shapeRef = React.useRef<any | undefined>();
  const trRef = React.useRef<any | undefined>();

  const rectMouseStyleHandler = useRectMouseStyle();
  const transMouseStyleHandler = useTransMouseStyle();

  React.useEffect(() => {
    // we need to attach transformer manually
    trRef.current.nodes([shapeRef.current]);
    trRef.current.getLayer().batchDraw();
  }, []);

  return (
    <>
      <Rect
        draggable={true}
        dragBoundFunc={(pos) => {
          setAnnotations(annotations.map(BoundingBoxes => {
            if (BoundingBoxes.id === id) {
              BoundingBoxes.x = (pos.x - stageOffsetX) / scale;
              BoundingBoxes.y = (pos.y - stageOffsetY) / scale;
            }
            return BoundingBoxes
          }))
          return pos
        }}
        ref={shapeRef}
        strokeWidth={1 / scale}
        x={x}
        y={y}
        width={width}
        height={height}
        fill="transparent"
        stroke="black"
        onTransformEnd={(event) => {
          const node = shapeRef.current;
          const scaleX = node.scaleX();
          const scaleY = node.scaleY();

          node.scaleX(1);
          node.scaleY(1);
          setAnnotations(annotations.map(BoundingBoxes => {
            if (BoundingBoxes.id === id) {
              BoundingBoxes.x = node.x();
              BoundingBoxes.y = node.y();
              BoundingBoxes.width = node.width() * scaleX;
              BoundingBoxes.height = node.height() * scaleY;
            }
            return BoundingBoxes
          }));
        }}
        {...rectMouseStyleHandler}
      />
      <Transformer
        ref={trRef}
        rotateEnabled={false}
        borderEnabled={false}
        ignoreStroke={true}
        keepRatio={false}
        {...transMouseStyleHandler}
      />
    </>
  );
};
