import { KonvaEventObject } from "konva/lib/Node";
import { useMouseStatus } from '../../context/mouseStatus';

export function useMouseStyle() {
  const { status, setStatus } = useMouseStatus();

  const onMouseEnter = (event: KonvaEventObject<MouseEvent>) => {
    if (status !== "draw") {
      setStatus("drag");
    }
  }

  const onMouseLeave = (event: KonvaEventObject<MouseEvent>) => {
    if (status !== "draw") {
      setStatus("move");
    }
  }

  return { onMouseEnter, onMouseLeave }
}