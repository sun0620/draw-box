import { KonvaEventObject } from "konva/lib/Node";
import { useMouseStatus } from '../../context/mouseStatus';
import { setMouseStyle } from '../../helper/common';

export function useMouseStyle() {
  const { status, setStatus } = useMouseStatus();

  const onMouseEnter = (event: KonvaEventObject<MouseEvent>) => {
    if (status !== "draw") {
      setMouseStyle(event, 'move');
      setStatus("drag");
    }
  }

  const onMouseLeave = (event: KonvaEventObject<MouseEvent>) => {
    if (status !== "draw") {
      setMouseStyle(event, 'crosshair');
      setStatus("move");
    }
  }

  return { onMouseEnter, onMouseLeave }
}