import React, { useState } from "react";
import useImage from "use-image";
import { PolygonCanvas } from "../polygon-canvas";
import { UploadImage } from "../upload-image";
import { StageProvider } from '../context/stage';
import { AnnotationsMatrixProvider } from '../context/annotations-matrix';
import { MouseStatusProvider } from '../context/mouseStatus';

const defaultImagePath = "/placeholder.png";

export const Polygon: React.FC<{}> = () => {
  const [imageUri, setImageUri] = useState<string>(defaultImagePath);
  const [image] = useImage(imageUri);

  return (
    <>
      <StageProvider>
        <MouseStatusProvider>
          <AnnotationsMatrixProvider>
            <PolygonCanvas image={image} />
          </AnnotationsMatrixProvider>
        </MouseStatusProvider>
      </StageProvider>
      <UploadImage setImageUri={setImageUri} />
    </>
  );
};
