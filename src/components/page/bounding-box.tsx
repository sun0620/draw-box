import React, { useState } from "react";
import useImage from "use-image";
import { BoundingBoxCanvas } from "../bounding-box-canvas";
import { UploadImage } from "../upload-image";
import { StageProvider } from '../context/stage';
import { AnnotationsProvider } from '../context/annotations';
import { MouseStatusProvider } from '../context/mouseStatus';

const defaultImagePath = "/placeholder.png";

export const BoundingBox: React.FC<{}> = () => {
  const [imageUri, setImageUri] = useState<string>(defaultImagePath);
  const [image] = useImage(imageUri);

  return (
    <>
      <StageProvider>
        <MouseStatusProvider>
          <AnnotationsProvider>
            <BoundingBoxCanvas image={image} />
          </AnnotationsProvider>
        </MouseStatusProvider>
      </StageProvider>
      <UploadImage setImageUri={setImageUri} />
    </>
  );
};
