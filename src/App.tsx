import { AppShell, Navbar, Header, Button } from '@mantine/core';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import { BoundingBox } from './components/page/bounding-box';
import { Polygon } from './components/page/polygon';

function App() {
  return (
    <BrowserRouter>
      <AppShell
        padding="md"
        navbar={
          <Navbar height={600} padding="xs" width={{ base: 300 }}>
          <Navbar.Section>
            <Button variant="subtle" uppercase fullWidth component={Link} to="/">home</Button>
            <Button variant="subtle" uppercase fullWidth component={Link} to="/bounding-box">bounding box</Button>
            <Button variant="subtle" uppercase fullWidth component={Link} to="/polygon">polygon</Button>
          </Navbar.Section>
        </Navbar>
        }
        header={<Header height={60} padding="xs">{/* Header content */}</Header>}
        styles={(theme) => ({
          main: { backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0] },
        })}
        >
        <Routes>
          <Route path="/"/>
          <Route path="/bounding-box" element={<BoundingBox/>}/>
          <Route path="/polygon" element={<Polygon/>}/>
        </Routes>
      </AppShell>
    </BrowserRouter>
  );
};

export default App;
